const {gameServiceEmitter} = require('../services_interface/game');

const gameSocketEvents = (socket) => {
    socket.on('key_w', data=>{
        gameServiceEmitter.userKeyPres(socket.id,{val:data,key:'w'});
    });

    socket.on('key_s', data=>{
        gameServiceEmitter.userKeyPres(socket.id,{val:data,key:'s'});
    });

    socket.on('key_d', data=>{
        gameServiceEmitter.userKeyPres(socket.id,{val:data,key:'d'});
    });

    socket.on('key_a', data=>{
        gameServiceEmitter.userKeyPres(socket.id,{val:data,key:'a'});
    });

    socket.on('fire', data=>{
        gameServiceEmitter.userKeyPres(socket.id,{val:data,key:'fire'});
    });

    socket.on('key_one', data=>{
        gameServiceEmitter.userKeyPres(socket.id,{val:data,key:'key_one'});
    });

    socket.on('mouse_move',data=>{
        gameServiceEmitter.userMouseMove(socket.id,data);
    });
}

const gameSocketEventsOff = (socket)=>{
    socket.removeAllListeners(['key_w','key_s','key_d','key_a','mouse_move','fire','key_one']);
    //socket.off('key_s');
}

module.exports = { gameSocketEvents, gameSocketEventsOff };