const userSocketEvents = (socket, roomsService, userService, gamesService) => {
    //console.log('создаю события для = ', socket.id);
    socket.on('create_room', (request) => {
        //console.log('в create_room');
        request['id'] = socket.id;
        const data = roomsService.createRoom(request);
        if (data) {
            //console.log('data = ', data);
            socket.to('rooms_room').emit('update_rooms', data.rooms);
            const room_name = 'r_' + socket.id;
            userService.fromToRoom(socket.id,'rooms_room',room_name);
            //socket.leave('rooms_room');
            //socket.join(room_name);
            const players = userService.getUsers(data.room.players);
            const t_pl = [...data.room.players];
            //const response = {...data.room, players};
            data.room['players'] = players;
            socket.emit('update_room', data.room);
            if(data.room.max_players<=data.room.players.length){
                //console.log('должен удалить комнату и создать игру');
                gamesService.createGame({id:data.room.id,players:t_pl,type:data.room.type});
                roomsService.delRoom(data.room.id);
                //gameServiceEmitter.emitter.emit('create_game', {id:data.id,players:data.players});
                //roomServiceEmitter.emitter.emit('del_room', room.id);
                
            }
            //console.log("создал комнату и отдал ее юзеру!!! = ", data.room);
        }
    });

    socket.on('join_room', (request) => {
        const data = roomsService.joinRoom(request);
        if (!data) {
            return;
        }
        //socket.leave('rooms_room');
        const room_name = 'r_' + data.id;
        //socket.join(room_name);
        userService.fromToRoom(socket.id,'rooms_room',room_name);
        const players = userService.getUsers(data.players);
        const t_pl = [...data.players];
        data['players'] = players;
        socket.emit('join_room', data);
        socket.to('r_' + data.id).emit('update_room', data);
        //console.log('data = ', data);
        if(data.max_players<=data.players.length){
            //console.log('должен удалить комнату и создать игру');
            gamesService.createGame({id:data.id,players:t_pl,type:data.type});
            roomsService.delRoom(data.id);
            //gameServiceEmitter.emitter.emit('create_game', {id:data.id,players:data.players});
            //roomServiceEmitter.emitter.emit('del_room', room.id);
            
        }
    });

    socket.on('get_rooms', () => {
        //console.log('отдаю комнаты!!!');
        const rooms = roomsService.getRooms();
        if (rooms) {
            socket.emit('update_rooms', rooms);
        }
    });

    socket.on('disconnect', () => {
        //console.log('поймал дисконект = ', socket.id);
        const user = userService.disconnect(socket.id);
        if(user){
            if(user.room!==null){
                const room = roomsService.getRoom(user.room);
                //console.log('room = ',room);
                if(room){
                    room['players'] = userService.getUsers(room.players)
                    socket.to('r_' + user.room).emit('update_room', room);
                }
            }
        }
    });

    socket.on('connect_room', data => {

    });

    // socket.on('key_w', data=>{
    //     gamesService.userKeyPres(socket.id,{val:data,key:'w'});
    // });

    // socket.on('key_s', data=>{
    //     gamesService.userKeyPres(socket.id,{val:data,key:'s'});
    // });
}

module.exports = { userSocketEvents };