let gameService = null;

class GameServiceEmitter{
    constructor(){
    }

    init(instance){
        gameService = instance;
    }

    createGame(data){
        if(gameService){
            gameService.createGame(data);
        }
    }

    getUserDopInfo(player_id){
        if(gameService){
            return gameService.getUserDopInfo(player_id);
        }
    }

    gameUpdate(id){
        if(gameService){
            gameService.gameUpdate(id);
        }
    }

    addBullet(data){
        if(gameService){
            gameService.addBullet(data);
        }
    }

    delBullet(data){
        if(gameService){
            gameService.delBullet(data);
        }
    }

    gameStart(id){
        if(gameService){
            gameService.gameStart(id);
        }
    }

    getGame(id){
        if(gameService){
            return gameService.getGame(id);
        }
    }

    gameOver(id){
        if(gameService){
            gameService.gameOver(id);
        }
    }

    userKeyPres(socket_id, data){
        if(gameService){
            //console.log('gameService.userKeyPres = ', data);
            gameService.userKeyPres(socket_id,data);
        }
    }

    userMouseMove(socket_id, data){
        if(gameService){
            //console.log('gameService.userKeyPres = ', data);
            gameService.userMouseMove(socket_id,data);
        }
    }
}

const gameServiceEmitter = new GameServiceEmitter();

module.exports = {gameServiceEmitter};
// export function getInstance(){
//   return instance;
// }