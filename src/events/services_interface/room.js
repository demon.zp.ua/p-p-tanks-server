let roomService = null;

class RoomServiceEmitter{
    constructor(){
    }

    init(instance){
        roomService = instance;
    }

    delRoom(data){
        if(roomService){
            roomService.delRoom(data);
        }
    }

    delUser(data){
        if(roomService){
            roomService.delUser(data);
        }
    }
}

const roomServiceEmitter = new RoomServiceEmitter();

module.exports = {roomServiceEmitter};
// export function getInstance(){
//   return instance;
// }