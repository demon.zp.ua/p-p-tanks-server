let userService = null;
class UserServiceEmitter{
    constructor(){
    }

    init(instance){
        userService = instance;
    }

    inRoom(data){
        if(userService){
            userService.addRoom(data);
        }
    }

    delRoom(data){
        if(userService){
            userService.delRoom(data);
        }
    }

    fromRoomToGame(data){
        if(userService){
            userService.fromRoomToGame(data);
        }
    }

    userEmit(data){
        if(userService){
            userService.userEmit(data);
        }
    }

    getUser(socket_id){
        if(userService){
            return userService.getUser(socket_id);
        }
    }

    getUserDopInfo(id){
        if(userService){
            return userService.getUserDopInfo(id);
        }
    }

    gameOver(data){
        if(userService){
            return userService.gameOver(data);
        }
    }
}

const userServiceEmitter = new UserServiceEmitter();

module.exports = {userServiceEmitter};