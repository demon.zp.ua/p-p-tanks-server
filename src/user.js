module.exports = class User{
    constructor({id, name, client, socket}){
        this.id = id;
        this.name = name;
        this.socket = socket;
        this.socket_id = socket.id;
        this.client = client;
        //console.log("socket = ", socket);
        this.room = null;
        this.game = null;
        this.online = true;
        this.is_reconnect = false;
    }

    reconnect(socket){
        if(this.online){
            this.is_reconnect = true;
            try{
                this.socket.disconnect(true);
            }catch(error){
                return false;
                //console.log("ошибка дисконнекта Юзера = ", error);
            }
        }
        this.socket_id = socket.id;
        this.online = true;
        this.socket = socket;
        return true;
    }

    getDopInfo(){
        return {
            name:this.name,
            client:this.client
        };
    }

    gameOver(){
        this.fromToRoom('g_'+this.game,'rooms_room');
        this.delGame();
    }

    addRoom(room){
        this.room = room;
    }

    addGame(game){
        this.game = game;
    }

    delGame(){
        this.game = null;
    }

    delRoom(){
        this.room = null;
    }

    toRoom(to){
        this.socket.join(to);
    }

    fromToRoom(from,to){
        this.socket.leave(from);
        this.socket.join(to);
    }

    socketEmit(e,data){
        this.socket.emit(e,data);
    }

    disconnect(){
        
    }

    apiJson(){
        return{
            name:this.name
        }
    }

    toJson(){
        const t_user = {
            id: this.id,
            name: this.name,
            socket_id: this.socket_id,
            room: this.room,
            game: this.game
        }

        return t_user;
    }
}