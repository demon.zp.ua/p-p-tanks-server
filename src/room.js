const {gameServiceEmitter} = require('./events/services_interface/game');
const {userServiceEmitter} = require('./events/services_interface/user');
const {roomServiceEmitter} = require('./events/services_interface/room');

module.exports = class Room{
    constructor({id, max_players,type, player}){
        this.type = type;
        this.max_players = max_players;
        if(max_players<2&&type!=='PVE'){
            this.max_players = 2;
        }
        if(max_players>3){
            this.max_players = 3;
        }
        //this.num_players = 0;
        this.players = [];
        //console.log('player = ', player);
        this.players.push(player);
        //this.num_players++;
        this.id = id;
    }

    delRoom(){
        this.players.forEach(player => {
            //console.log('this.players = ', this.players);
            userServiceEmitter.delRoom(player);
            //userServiceEmitter.emitter.emit('del_room', player);
        });
    }

    addUser(user){
        if(this.max_players<=this.players.length){
            return false;
        }
        for(let i=0;i<this.players.length;i++){
            //console.log(this.players[i]+'||'+user);
            if(this.players[i]==user){
                //console.log('такой юзер найден должен выкинуть!!!!');
                return false;
            }
        }
        this.players.push(user);
        
        return true;
    }

    delUser(user){
        for(let i=0;i<this.players.length;i++){
            if(this.players[i]==user){
                this.players.splice(i,1);
                break;
            }
        }
        //console.log("this.players.length = ", this.players.length);
        if(this.players.length<=0){
            //console.log("должен удалить комнату");
            roomServiceEmitter.delRoom(this.id);
        }
    }

    async update(){
        let data = {id:this.id};
        if(this.max_players>=this.num_players){
            gameServiceEmitter.emitter.emit('create_game', data);
        }
    }
}