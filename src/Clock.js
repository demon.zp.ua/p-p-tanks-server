module.exports = class Clock{
    constructor(update){
        this._update = update;
        this._timers = [];
        //this._realTime = Date.now();
        //this._lastTime = 0;
        this._time = 0;
        this.t_time = 0;
        this._delta = 0;
        this.t_delta = 0;
        this.time = Date.now();
        this._fps = Math.floor(1000/60);
        //this._fps = 1000*10;
        this._timer = setInterval(this.update, this._fps);
    }

    clear(){
        clearInterval(this._timer);
    }

    addTimer(time,func){
        //console.log('добавил таймер!!!');
        this._timers.push({time,timer:0,func});
    }

    _delTimer(i){
        this._timers.splice(i,1);
    }

    _isTime(){
        //let kill = [];
        for(let i=0;i<this._timers.length;i++){
            
            if(Math.abs(this._timers[i].timer)>=this._timers[i].time){
                this._timers[i].func();
                this._timers.splice(i,1);
                i--;
                //kill.push(i);
            }
        }
        // this._timers.forEach((el,i) => {
        //     //console.log('timer = ',el.timer,'||',el.time);
        //     if(Math.abs(el.timer)>=el.time){
        //         el.func();
        //         kill.push(i);
        //     }
        // });

        // kill.forEach((el,i)=>{
        //     this._delTimer(i);
        // });
    }

    update = ()=>{
        this.t_time = Date.now();
        this.t_delta = this.t_time - this.time;
        this.delta = this._fps - this.t_delta;

        let time = this._fps - this.delta;

        this.time += time;
        this._time += time;

        this._timers.forEach(el=>{
            el.timer += time;
        });

        this._isTime();

        this._update(this.delta);
    }
}