const {gameServiceEmitter} = require('./events/services_interface/game');
const Tank = require('./game_objs/Tank');
const Wall = require('./game_objs/Wall');
const Bullet = require('./game_objs/Bullet');
const Clock = require('./Clock');
const WorldBorder = require('./game_objs/WorldBorder');
const {makeId} = require('./serveces/Mth');

module.exports = class Game{
    constructor({id, players,type}){
        //console.log('создал игру!!! = ', id,'/',players);
        this.players = players;
        this._map_tanks = {};
        this.tanks = {};
        this.status = 'init';
        this.type = type;
        this.reason_over = '';
        this.winner_id = null;
        //this.num_players++;
        this.id = id;
        this.mainTimer = null;
        //this.send = true;
        this.time = 0;
        this._time_battle = 10*60*1000;
        this.realTime = Date.now();
        this.lastTime = 0;
        this.is_start = false;
        this.width_min = 20;
        this.width_max = 900;
        this.height_min = 20;
        this.height_max = 700;
        this.map_walls = [
            {x:200,y:240,w:20,h:60},
            {x:360,y:200,w:60,h:20},
            {x:40,y:450,w:40,h:20},
            {x:400,y:660,w:20,h:80},
            {x:240,y:490,w:20,h:40},
            {x:640,y:540,w:20,h:40},
            {x:670,y:400,w:90,h:20},
            {x:400,y:420,w:20,h:20},
            {x:540,y:200,w:20,h:20},
            {x:580,y:220,w:20,h:40},
            {x:400,y:60,w:20,h:40},
            {x:680,y:100,w:20,h:20},
            {x:90,y:280,w:20,h:20}
            // {x:240,y:200},
            // {x:260,y:200},
            // {x:280,y:200},
            // {x:300,y:300},
            // {x:200,y:240},
            // {x:200,y:260},
            // {x:200,y:280},
            // {x:200,y:380},
        ];
        this.walls = [];
        this._t_pos = [
            {
                x:100,
                y:100,
                angle:180
            },
            {
                x:800,
                y:600,
                angle:0
            },
            {
                x:100,
                y:600,
                angle:180
            }
        ];
        
        this.bullets = [];
        this.bonuses = [];
        this._num_tanks = players.length;

        this.init();
        // this._time = this.realTime;
        // this._lastTime = 0;
        // this._fps = Math.floor(1000/60);
        //let begin_timer = setInterval(this.init, 2000);
        //this.beginTimer = setTimeout(this.init, 1000*30);
        //this.mainTimer = setInterval(this.update, this._fps);
        //this.mainTimer = setTimeout(this.update, this._fps);
    }

    init(){
        WorldBorder.init({
            width_min:this.width_min,
            width_max:this.width_max,
            height_min:this.height_min,
            height_max:this.height_max,
            width_bullet:10/2,
            width_tank:40/2
        });
        this.map_walls.forEach(wall=>{
            this.walls.push(new Wall(wall));
        });
        //this.walls.push(new Wall({x:200,y:200}));
        this.mainTimer = new Clock(this.update);
        this.createTanks();
        this.status = 'countdown';
        this.mainTimer.addTimer(1000*3,this.start);
        this.mainTimer.addTimer(this._time_battle,this.timeIsOver);
    }

    createTanks(){
        this.players.forEach(player => {
            let r_str = makeId();
            this._map_tanks[player] = r_str;
            let pos = Math.floor(Math.random() * this._t_pos.length);
            const dop_info = gameServiceEmitter.getUserDopInfo(player);
            this.tanks[r_str] = new Tank({
                ...this._t_pos[pos],
                ...dop_info,
                id:r_str,
                timer:this.mainTimer,
                walls:this.walls,
                addBullet:this.addBullet
            });
            this._t_pos.splice(pos,1);
        });

        for(const user in this._map_tanks){
            this.players.forEach(pl=>{
                if(pl!=user){
                    this.tanks[this._map_tanks[user]].addEnemy(pl);
                }
            });
        }
        //console.log('this.tanks = ', this.tanks);
        //gameServiceEmitter.gameUpdate(this.id);
    }

    killTank = (id)=>{
        this._num_tanks --;
        //console.log('this._num_tanks = ', this._num_tanks);
        if(this._num_tanks<=1 && this.status!='game_over'){
            this.status = 'game_over';
            this.reason_over = 'winner';
            this.winner_id = this.findWinner();
            this.mainTimer.clear();
            gameServiceEmitter.gameOver(this.id);
            //console.log('противники уничтожены!!!');
        }
    }

    findWinner(){
        for(const user in this._map_tanks){
            if(this.tanks[this._map_tanks[user]].hit_points>0){
                return user;
            }
        }
    }
    
    timeIsOver = ()=>{
        this.status = 'game_over';
        this.reason_over = 'time_out';
        this.mainTimer.clear();
        gameServiceEmitter.gameOver(this.id);
        //console.log('время вышло!!');
    }

    start = () => {
        this.lastTime = Date.now();
        //console.log('прошло 30 секунд!!! = ', this.id);
        //clearTimeout(this.beginTimer);
        this.status = 'battle';
        gameServiceEmitter.gameStart(this.id);
        this.is_start = true;
        //this.mainTimer.clear();
        //clearInterval(this.mainTimer);
        //this.mainTimer = setInterval(this.update, Math.floor(1000/60));
    }

    toJsonGameOver(user_id){
        //console.log('user_id = ', user_id);
        return{
            ...this.toUser(user_id),
            status:this.status,
            reason_over: this.reason_over,
            winner:this.winner_id,
            lader_board:this._getLaderBoard()
        };
    }

    toJson(){
        return {
            id:this.id,
            time:this.time,
            type:this.type,
            status:this.status,
            reason_over: this.reason_over,
            winner:this.winner_id,
            width:this.width,
            height:this.height,
            lader_board:[]
        };
    }

    toUser(user_id){
        
        return {
            time:this.time,
            status:this.status,
            player:this.tanks[this._map_tanks[user_id]].toJsonPl(),
            bullets:this.getBullets(),
            enemys:this.getEnemys(user_id),
            allies:this.getAllies(user_id),
            bonuses:this.getBonuses()
        }
    }

    getBonuses(){
        const t_bonuses = this.bonuses.map((b)=>{
            return b.toJson();
        });
        return t_bonuses;
    }

    getEnemys(user_id){
        let enemys = [];
        this.tanks[this._map_tanks[user_id]]._enemys.forEach(pl=>{
            enemys.push(this.tanks[this._map_tanks[pl]].toJson());
        });
        //console.log(this.tanks[this._map_tanks[user_id]]._enemys,'||',enemys);
        return enemys;
    }

    getAllies(user_id){
        let allies = [];
        this.tanks[this._map_tanks[user_id]].allies.forEach(pl=>{
            allies.push(this.tanks[this._map_tanks[pl]].toJson());
        });
        //console.log(this.tanks[this._map_tanks[user_id]]._enemys,'||',enemys);
        return allies;
    }

    getBullets(){
        const t_bullets = this.bullets.map((b)=>{
            return b.toJson();
        });
        return t_bullets;
        // this.bullets.forEach(b=>{
        //     b.toJson();
        // });
    }

    _getLaderBoard(){
        const lader = [];
        for(const key in this.tanks){
            lader.push(this.tanks[key].toLader());
        }
        return lader;
    }

    isGameOver(){

    }

    userKeyPres(user_id,data){
        
        this.tanks[this._map_tanks[user_id]].keyPres(data);
    }

    userMouseMove(user_id,data){
        this.tanks[this._map_tanks[user_id]].mouseMove(data);
    }

    addBullet = (data)=>{
        let bullet = new Bullet({
            ...data,
            tanks:this.tanks,
            id:makeId(),
            delBullet:this.delBullet,
            killTank:this.killTank
        });
        //this.arr_add_bullets.push(bullet);
        //gameServiceEmitter.addBullet({id:this.id,bullet:bullet.toJson()});
        this.bullets.push(bullet);
        //console.log('должен бабахнуть!!!');
    }

    delBullet = (id)=>{
        const idx = this.bullets.findIndex((b)=>{return b.id===id});
        //gameServiceEmitter.delBullet({id:this.id,bullet:this.bullets[idx].toJson()});
        this.bullets.splice(idx,1);
    }

    update = (delta)=>{
        // let time = Date.now();
        // let delta = time - this._time;

        // this._lastTime += delta;
        // this._time = time;
        // let t_delta = this._fps - delta;
        // this.time += this._fps - t_delta;
        // gameServiceEmitter.gameUpdate(this.id);
        // if(Math.abs(this._lastTime)>= 1000*30){
        //     this.init();
        // }
        this.time = this.mainTimer._time;
        if(this.is_start){
            this.isGameOver();
            for(const key in this.tanks){
                this.tanks[key].update(delta);
            }

            this.bullets.forEach(b=>{
                b.update(delta);
            });
        }
        gameServiceEmitter.gameUpdate(this.id);
        //this.mainTimer.clear();
    }
}