const {gameServiceEmitter} = require('./events/services_interface/game');
const TankPVE = require('./game_objs/TankPVE');
const Enemys = require('./game_objs/Enemys');
const Wall = require('./game_objs/Wall');
const BulletPVE = require('./game_objs/BulletPVE');
const BonusObject = require('./game_objs/BonusObject');
const Clock = require('./Clock');
const WorldBorder = require('./game_objs/WorldBorder');
const {makeId} = require('./serveces/Mth');

module.exports = class GamePVE{
    constructor({id, players,type}){
        //console.log('создал PVE игру!!! = ', id,'/',players);
        this.players = players;
        this.type = type;
        this._map_teamA = {};
        //this.tanks = {};
        this.teamA = {};
        this.teamB = null;
        this.teamA_members = this.players;
        this.status = 'init';
        this.reason_over = '';
        this.winner_id = null;
        //this.num_players++;
        this.id = id;
        this.mainTimer = null;
        //this.send = true;
        this.time = 0;
        this._time_battle = 10*60*1000;
        this.realTime = Date.now();
        this.lastTime = 0;
        this.is_start = false;
        this.width_min = 20;
        this.width_max = 900;
        this.height_min = 20;
        this.height_max = 700;
        this.map_walls = [
            {x:200,y:240,w:20,h:60},
            {x:360,y:200,w:60,h:20},
            {x:40,y:450,w:40,h:20},
            {x:400,y:660,w:20,h:80},
            {x:240,y:490,w:20,h:40},
            {x:640,y:540,w:20,h:40},
            {x:670,y:400,w:90,h:20},
            {x:400,y:420,w:20,h:20},
            {x:540,y:200,w:20,h:20},
            {x:580,y:220,w:20,h:40},
            {x:400,y:60,w:20,h:40},
            {x:680,y:100,w:20,h:20},
            {x:90,y:280,w:20,h:20}
        ];
        this.walls = [];
        this._t_pos = [
            {
                x:340,
                y:300,
                angle:30
            },
            {
                x:500,
                y:300,
                angle:150
            },
            {
                x:340,
                y:420,
                angle:-30
            }
        ];
        
        this.bullets = [];
        this.bonuses = [];
        this.arr_add_bullets = [];
        this._num_tanks = players.length;

        this.init();
        // this._time = this.realTime;
        // this._lastTime = 0;
        // this._fps = Math.floor(1000/60);
        //let begin_timer = setInterval(this.init, 2000);
        //this.beginTimer = setTimeout(this.init, 1000*30);
        //this.mainTimer = setInterval(this.update, this._fps);
        //this.mainTimer = setTimeout(this.update, this._fps);
    }

    init(){
        WorldBorder.init({
            width_min:this.width_min,
            width_max:this.width_max,
            height_min:this.height_min,
            height_max:this.height_max,
            width_bullet:10/2,
            width_tank:40/2
        });
        this.map_walls.forEach(wall=>{
            this.walls.push(new Wall(wall));
        });
        //this.walls.push(new Wall({x:200,y:200}));
        this.mainTimer = new Clock(this.update);
        this.createPlayersTeam();
        this.teamB = new Enemys({
            walls:this.walls,
            timer:this.mainTimer,
            addBullet:this.addBullet,
            addBonus:this.addBonus,
            playersWon:this.playersWon
        });
        this.status = 'countdown';
        this.mainTimer.addTimer(1000*3,this.start);
        this.mainTimer.addTimer(this._time_battle,this.timeIsOver);
    }

    createPlayersTeam(){
        this.teamA_members.forEach(player => {
            let r_str ='t'+makeId();
            this._map_teamA[player] = r_str;
            let pos = Math.floor(Math.random() * this._t_pos.length);
            const dop_info = gameServiceEmitter.getUserDopInfo(player);
            this.teamA[r_str] = new TankPVE({
                ...this._t_pos[pos],
                ...dop_info,
                id:r_str,
                timer:this.mainTimer,
                walls:this.walls,
                addBullet:this.addBullet
            });
            this._t_pos.splice(pos,1);
        });

        for(const user in this._map_teamA){
            this.players.forEach(pl=>{
                if(pl!=user){
                    this.teamA[this._map_teamA[user]].addAlly(pl);
                }
            });
        }
        //console.log('this.tanks = ', this.tanks);
        //gameServiceEmitter.gameUpdate(this.id);
    }

    playersWon = ()=>{
        this.status = 'game_over';
        this.reason_over = 'players_won';
        this.mainTimer.clear();
        gameServiceEmitter.gameOver(this.id);
    }

    killTank = (id)=>{
        this._num_tanks --;
        //console.log('this._num_tanks = ', this._num_tanks);
        if(this._num_tanks<=0 && this.status!='game_over'){
            this.status = 'game_over';
            this.reason_over = 'winner';
            this.winner_id = 'Coronavirus';
            this.mainTimer.clear();
            gameServiceEmitter.gameOver(this.id);
            //console.log('противники уничтожены!!!');
        }
    }

    findWinner(){
        for(const user in this._map_teamA){
            if(this.teamA[this._map_teamA[user]].hit_points>0){
                return user;
            }
        }
    }
    
    timeIsOver = ()=>{
        this.status = 'game_over';
        this.reason_over = 'time_out';
        this.mainTimer.clear();
        gameServiceEmitter.gameOver(this.id);
        //console.log('время вышло!!');
    }

    start = () => {
        this.lastTime = Date.now();
        //console.log('прошло 30 секунд!!! = ', this.id);
        //clearTimeout(this.beginTimer);
        this.status = 'battle';
        gameServiceEmitter.gameStart(this.id);
        this.is_start = true;
        //this.mainTimer.clear();
        //clearInterval(this.mainTimer);
        //this.mainTimer = setInterval(this.update, Math.floor(1000/60));
    }

    toJsonGameOver(user_id){
        //console.log('user_id = ', user_id);
        return{
            ...this.toUser(user_id),
            status:this.status,
            reason_over: this.reason_over,
            winner:this.winner_id,
            lader_board:this._getLaderBoard()
        };
    }

    toJson(){
        return {
            id:this.id,
            time:this.time,
            type:this.type,
            status:this.status,
            reason_over: this.reason_over,
            winner:this.winner_id,
            width:this.width,
            height:this.height,
            lader_board:[]
        };
    }

    toUser(user_id){
        
        return {
            time:this.time,
            status:this.status,
            player:this.teamA[this._map_teamA[user_id]].toJsonPl(),
            bullets:this.getBullets(),
            enemys:this.teamB.getEnemys(),
            allies:this.getAllies(user_id),
            bonuses:this.getBonuses()
        }
    }

    getAllies(user_id){
        let allies = [];
        this.teamA[this._map_teamA[user_id]].allies.forEach(pl=>{
            allies.push(this.teamA[this._map_teamA[pl]].toJson());
        });
        //console.log(this.tanks[this._map_tanks[user_id]]._enemys,'||',enemys);
        return allies;
    }

    getBullets(){
        const t_bullets = this.bullets.map((b)=>{
            return b.toJson();
        });
        return t_bullets;
        // this.bullets.forEach(b=>{
        //     b.toJson();
        // });
    }

    getBonuses(){
        const t_bonuses = this.bonuses.map((b)=>{
            return b.toJson();
        });
        return t_bonuses;
        // this.bullets.forEach(b=>{
        //     b.toJson();
        // });
    }

    _getLaderBoard(){
        const lader = [];
        for(const key in this.teamA){
            lader.push(this.teamA[key].toLader());
        }
        lader.push(this.teamB.toLader());
        return lader;
    }

    isGameOver(){

    }

    userKeyPres(user_id,data){
        
        this.teamA[this._map_teamA[user_id]].keyPres(data);
    }

    userMouseMove(user_id,data){
        this.teamA[this._map_teamA[user_id]].mouseMove(data);
    }

    addBullet = (data)=>{
        const bullet = new BulletPVE({
            ...data,
            tanks:this.teamA,
            enemys:this.teamB,
            id:makeId(),
            delBullet:this.delBullet,
            killTank:this.killTank
        });
        //this.arr_add_bullets.push(bullet);
        //gameServiceEmitter.addBullet({id:this.id,bullet:bullet.toJson()});
        this.bullets.push(bullet);
        //console.log('должен бабахнуть!!!');
    }

    delBullet = (id)=>{
        const idx = this.bullets.findIndex((b)=>{return b.id===id});
        //gameServiceEmitter.delBullet({id:this.id,bullet:this.bullets[idx].toJson()});
        this.bullets.splice(idx,1);
    }

    addBonus = (data)=>{
        const bonus = new BonusObject({
            ...data,
            bonus:'bonus_heal',
            tanks:this.teamA,
            id:'b'+makeId(),
            delBonus:this.delBonus
        });
        this.bonuses.push(bonus);
    }

    delBonus = (id)=>{
        const idx = this.bonuses.findIndex((b)=>{return b.id===id});
        this.bonuses.splice(idx,1);
    }

    update = (delta)=>{
        this.time = this.mainTimer._time;
        if(this.is_start){
            this.isGameOver();
            for(const key in this.teamA){
                this.teamA[key].update(delta);
            }

            this.teamB.update(delta);

            this.bullets.forEach(b=>{
                b.update(delta);
            });

            this.bonuses.forEach(b=>{
                b.update(delta);
            });
        }
        gameServiceEmitter.gameUpdate(this.id);
        //this.mainTimer.clear();
    }
}