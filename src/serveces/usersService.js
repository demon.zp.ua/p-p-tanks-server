//const {gameServiceEmitter} = require('../events/services_events/game');
const bd_users = require('../../db_user');
const User = require('../user');
const {userServiceEmitter} = require('../events/services_interface/user'); 
const {roomServiceEmitter} = require('../events/services_interface/room');
const {gameServiceEmitter} = require('../events/services_interface/game');
const {gameSocketEvents, gameSocketEventsOff} = require('../events/socket_events/game');

module.exports = class UsersService{
    constructor(io){
        this.io = io;
        this.users = {};
        this.emitter = userServiceEmitter.init(this);
    }

    createUser(user){
        this.users[user.id] = new User(user);
        return this.users[user.id];
    }

    authUser({name, password, client , socket}){
        let temp_user = bd_users.find(user=>user.name===name && user.password===password);
        if(temp_user===undefined){
            return undefined;
        }

        let user = this.findUser(temp_user.id);

        if(user){
            if(user.reconnect(socket)){
                return user;
            }
            return user;
        }
        //temp_user['socket'] = socket;
        user = this.createUser({
            ...temp_user,
            socket,
            client
        });
        //console.log('this.users = ', this.users);
        return user;
    }

    toRoom(user){
        //console.log('nen = ', user);
        if(user.game){
            //console.log('должен отослать оповещение Юзеру!!');
            user.socket.emit('start_game');
            user.socket.emit('init_game', gameServiceEmitter.getGame(user.game));
            user.toRoom('g_'+user.game);
            gameSocketEvents(user.socket);
            //gameSocketEventsOff(user.socket);
        }else{
            user.toRoom('rooms_room');
        }
    }

    gameOver(data){
        const user = this.findUser(data.id);
        gameSocketEventsOff(user.socket);
        user.gameOver();
        if(!user.online){
            this.disconnect(user.socket_id);
        }else{
            user.socketEmit('game_over',data.data);
        }
    }

    userEmit(data){
        const user = this.findUser(data.id);
        user.socketEmit(data.e,data.data);
    }

    getUsers(arr_id){
        //console.log('arr_id = ', arr_id);
        //console.log('this.users = ', this.users);
        const users = [];
        arr_id.forEach(el => {
            let user = this.findUser(el);
            if(user){
                users.push(user.toJson());
            }
        });
        //console.log('users= ', users);
        return users;
    }

    getUser(socket_id){
        const user = this.findBySocket(socket_id);
        if(user){
            return user.toJson();
        }
        return;
    }

    getUserDopInfo(id){
        const user = this.findUser(id);
        if(user){
            return user.getDopInfo();
        }
        return;
    }

    addRoom(data){
        const user = this.findUser(data.player);
        if(user){
            //console.log('должен вписать комнату Юзеру');
            user.addRoom(data.room);
        }
    }

    delRoom(id){
        //console.log('ищу юзера = ', id);
        const user = this.findUser(id);
        //console.log('удалил комнату у юзера = ', user.toJson());
        if(user){
            user.delRoom();
        }
        // arr_users.forEach(pl=>{
            
        // });
    }

    fromRoomToGame(data){
        const from = 'r_'+data.id;
        const to = 'g_'+data.id;

        data.players.forEach(pl=>{
            const player = this.findUser(pl);
            if(player){
                player.delRoom();
                player.addGame(data.id);
                player.fromToRoom(from,to);
                gameSocketEvents(player.socket);
            }
        });
    }

    fromToRoom(socket_id,from,to){
        const user = this.findBySocket(socket_id);
        if(user){
            user.fromToRoom(from,to);
        }
    }

    disconnect(socket_id){
        const user = this.findBySocket(socket_id);
        //console.log("user = ", user);
        let t_user = null;
        if(user){
            t_user = {...user};
            //console.log("оповещ удаление юзера в roomService и удаляю Юзера");
            roomServiceEmitter.delUser({user:user.id,room:user.room});
            if(!user.game && !user.is_reconnect){
                //console.log('удаляю из обьекта юзеров');
                delete this.users[user.id];
            }else{
                user.room = null;
                user.is_reconnect = false;
                user.online = false;
            }
            //const in_game = gameServiceEmitter.emitter.emit('find_user',{user,game:user.game});
            //console.log(in_game);
            // if(user.game){
            //     const in_game = gameServiceEmitter.emitter.emit('find_user',{user,game:user.game});
            // }
        }
        //console.log('this.users = ', this.users);
        return t_user;
    }

    findBySocket(id){
        for(let user in this.users){
            if(this.users[user].socket_id===id){
                return this.users[user];
            }
        }

        return;
    }

    findUser(id){
        if(id in this.users){
            return this.users[id];
        }
        return;
    }
}