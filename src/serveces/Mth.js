const random = (min,max)=>{
    return Math.floor(min + Math.random() * (max + 1 - min));
}

const makeId = ()=>{
    let text = "";
    let possible = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    for( let i=0; i < 6; i++ ){
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}

module.exports = {
    random,
    makeId
};