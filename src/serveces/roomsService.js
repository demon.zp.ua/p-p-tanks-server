const Room = require('../room');
const {userServiceEmitter} = require('../events/services_interface/user');
const {roomServiceEmitter} = require('../events/services_interface/room');

module.exports = class RoomsService{
    constructor(io){
        this.io = io;
        this.rooms = {};
        this.emitter = roomServiceEmitter.init(this);
    }

    createRoom(data){
        //console.log('должен создать комнату = ', data);
        const room = this.rooms[data.id] = new Room(data);
        userServiceEmitter.inRoom({room:data.id,player:data.player});
        //userServiceEmitter.emitter.emit('in_room', {room:data.id,player:data.player});
        return {rooms: {...this.rooms}, room:{...room}};
    }

    joinRoom(data){
        const room = this.findRoom(data.room_id);
        const is_add = room.addUser(data.user_id);
        if(!is_add){
            return false;
        }
        userServiceEmitter.inRoom({room:data.room_id,player:data.user_id});
        
        return {...room};
    }

    getRooms(){
        return this.rooms;
    }

    getRoom(id){
        let room = this.findRoom(id);
        if(room){
            return {...this.findRoom(id)};
        }
        return null;
    }

    delRoom(id){
        const room = this.findRoom(id);
        if(room){
            //userServiceEmitter.emitter.emit('del_room', room.players);
            room.delRoom();
            delete this.rooms[room.id];
            //console.log('this.rooms = ', this.rooms);
            this.io.to('rooms_room').emit('update_rooms', this.rooms);
        }
    }

    findRoom(id){
        if(id in this.rooms){
            return this.rooms[id];
        }
        return null;
    }

    delUser({user,room}){
        //console.log('должен сообщить в комнату удалить Юзера = ', user,' room= ', room);
        if(room){
            //console.log('сообщаю в комнату удалить Юзера');
            this.rooms[room].delUser(user);
        }
    }

    async update(){
        for(let room in this.rooms){
            this.rooms[room].update();
        }
    }
}