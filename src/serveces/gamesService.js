const Game = require('../game');
const GamePVE = require('../gamePVE');
const {gameServiceEmitter} = require('../events/services_interface/game');
const {userServiceEmitter} = require('../events/services_interface/user');

module.exports = class GamesService{
    constructor(io){
        this.io = io;
        this.games = {};
        this.emitter = gameServiceEmitter.init(this);
    }

    createGame(data){
        userServiceEmitter.fromRoomToGame(data);
        this.io.to('g_'+data.id).emit('start_game');
        if(data.type==='PVE'){
            this.games[data.id] = new GamePVE(data);
        }else{
            this.games[data.id] = new Game(data);
        }
        
        this.io.to('g_'+data.id).emit('init_game', this.games[data.id].toJson());
        // setTimeout(()=>{
        //     this.io.to('g_'+data.id).emit('init_game', this.games[data.id].toJson());
        // },5000);
        
        //userServiceEmitter.emitter.emit('from_room_to_game', data);
        //this.io.to('g_'+data.id).emit('start_game', this.games[data.id].toJson());
        //console.log('должен создать игру!!!! = ', data);
    }

    getGame(id){
        const game = this.games[id];
        if(game){
            return game.toJson();
        }
        return null;
    }

    getUserDopInfo(player_id){
        return userServiceEmitter.getUserDopInfo(player_id);
    }

    reconnect(user){
        this.games[user.game].reconnect(user);
    }

    gameStart(id){
        this.io.to('g_'+id).emit('game_start');
    }

    userKeyPres(socket_id,data){
        //console.log('userDownW = ', val);
        const user = userServiceEmitter.getUser(socket_id);
        try {
            this.games[user.game].userKeyPres(user.id,data);
        } catch (error) {
            
        }
        
    }

    userMouseMove(socket_id,data){
        const user = userServiceEmitter.getUser(socket_id);
        try {
            this.games[user.game].userMouseMove(user.id,data);
        } catch (error) {
            
        }
    }

    gameOver(id){
        //console.log('окончена игра = ', id);
        //console.log('игра = ',this.games[id].players);
        this.games[id].players.forEach(pl=>{
            userServiceEmitter.gameOver({
                id:pl,
                e:'game_over',
                data:{
                    ...this.games[id].toJsonGameOver(pl)
                }
            });
        });
        delete this.games[id];
        //console.log(this.games);
    }

    gameUpdate(id){
        //console.log(id,'||',this.games);
        if(this.games.hasOwnProperty(id)){
            this.games[id].players.forEach(pl=>{
                userServiceEmitter.userEmit({
                    id:pl,
                    e:'game_update',
                    data:this.games[id].toUser(pl)
                });
            });
        }
        //this.io.to('g_'+id).emit('game_update', {game:this.games[id].toJson()});
    }
}