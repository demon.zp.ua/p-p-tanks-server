const Tower = require('./Tower');
const WorldBorder = require('./WorldBorder');
const GameObject = require('./GameObject');

module.exports = class TankPVE extends GameObject{
    constructor({
                    id,
                    name,
                    x,
                    y,
                    angle,
                    client,
                    timer,
                    walls,
                    addBullet
                }){
        super({
                x,
                y,
                angle,
                type:'tank'
            });
        this.id = id;
        this.name = name;
        this.client = client;
        this.timer = timer;
        this.walls = walls;
        this.addBullet = addBullet;
        this.max_hit_points = 8;
        this.hit_points = this.max_hit_points;
        this.percent_hp = 100;
        this.num_kills = 0;
        this.damage_done = 0;
        this.damage_take = 0;
        this.heal_take = 0;
        this.heal_done = 0;
        this.damage = 1;
        this._angle = 0;
        this._live = true;
        this.target_pos = {x:0,y:0};
        //this.is_forward = false;
        //this.is_back = false;
        this._main_speed = 2.2;
        this._main_speed_rot = 1;
        this._speed_rot = 0;
        this._speed = 0;
        this._speed_bullet = 7.92;
        this._forse_y = 1;
        this._forse_x = 1;
        this.is_induced = false;
        this._enemys = [];
        this.allies = [];
        this.max_num_bonus = 1;
        this.bonuses = [];
        this._cd_fire = false;
        this._time_cd_fire = 1000*3;
        this._sprite_bullet = 'bullet1';
        this.sprite_bullet = this._sprite_bullet;
        //this.rotCorect = 180;
        this.tower = null;
        this.init();
    }

    init(){
        this.tower = new Tower(this);
        let flip = -1;
        if(this.angle==180){
            flip = 1;
        }
        this.target_pos = {
            x:this.x+(30*flip),
            y:this.y
        }
    }

    setHeal(damage){
        let t_heal = damage;
        this.hit_points += damage;

        if(this.hit_points>this.max_hit_points){
            t_heal -= this.hit_points - this.max_hit_points;
            this.hit_points = this.max_hit_points;
        }

        this.percent_hp = this.hit_points*100/this.max_hit_points;

        this.heal_take += t_heal;
        return t_heal;
    }

    setDoneHeal(heal){
        this.heal_done+=heal;
    }

    setDamage(damage){
        let t_damage = damage;
        this.hit_points -= damage;

        if(this.hit_points<0){
            t_damage += this.hit_points;
        }

        this.damage_take += t_damage;
        this.percent_hp = this.hit_points*100/this.max_hit_points;
        return t_damage;
    }

    setDoneDamage(damage){
        this.damage_done+=damage;
    }

    setKill(){
        this.num_kills++;
    }

    isKill(){
        if(this.hit_points<=0){
            this._live = false;
            return true;
            //this.killTank(this.id);
        }

        return false;
    }

    addEnemy(user_id){
        this._enemys.push(user_id);
    }

    addAlly(user_id){
        this.allies.push(user_id);
    }

    addBonus(bonus){
        if(this.max_num_bonus>this.bonuses.length){
            this.bonuses.push(bonus);
            //console.log('bonuses = ', this.bonuses); 
        }
    }

    onBonus(i){
        
        if(i>=this.bonuses.length){
            this.sprite_bullet = 'bullet_heal';
            this.bonuses.splice(i-1,1);
        }
        //console.log('услышал активацию бонуса!!! = ', this.bonuses);
    }

    mouseMove(data){
        if(this.client==='PC'){
            this.mouseMovePC(data);
        }else{
            this.mouseMoveMob(data);
        }
    }

    mouseMovePC(data){
        //console.log('двинули мыша!!');
        this.is_induced = false;
        //this.towerDAngle = data;
        this.target_pos = data;
        //this.tower.dAngle = data;
        //console.log('mouse = ', data);
    }

    mouseMoveMob(data){
        //console.log('двинули мыша!!');
        this.is_induced = false;
        //this.towerDAngle = data;
        //this.target_pos = data;
        this.tower.dAngle = data;
        //console.log('mouse = ', data);
    }

    keyPres(data){
        //console.log("data = ", data);
        switch (data.key) {
            case 'w':
                if(data.val){
                    this._speed = this._main_speed * -1;
                }else{
                    this._speed = 0;
                }
                break;
            case 's':
                if(data.val){
                    this._speed = this._main_speed;
                }else{
                    this._speed = 0;
                }
                //alert( 'В точку!' );
                break;
            case 'a':
                if(data.val){
                    this._speed_rot = this._main_speed_rot * -1;
                }else{
                    this._speed_rot = 0;
                }
              //alert( 'Перебор' );
              break;
            case 'd':
                if(data.val){
                    this._speed_rot = this._main_speed_rot;
                }else{
                    this._speed_rot = 0;
                }
              //alert( 'Перебор' );
              break;
            case 'fire':
                if(data.val){
                    this.fire();
                }
              //alert( 'Перебор' );
              break;
            case 'key_one':
                if(data.val){
                    this.onBonus(1);
                }
              //alert( 'Перебор' );
              break;
            default:
              //console.log( "Нет таких значений" );
        }
    }

    toLader(){
        return{
            id:this.id,
            name:this.name,
            damage_done:this.damage_done,
            damage_take:this.damage_take,
            heal_done:this.heal_done,
            heal_take:this.heal_take,
            num_kills:this.num_kills
        }
    }

    _toJson(){
        return {
            x:this.x,
            y:this.y,
            angle:this.angle,
            type:this.type,
            max_hit_points:this.max_hit_points,
            speed_rot:this._speed_rot,
            speed:this._speed,
            hit_points:this.hit_points,
            percent_hp:this.percent_hp,
            tower_angle:this.tower.angle
        };
    }

    toJsonPl(){
        return {
            ...this._toJson(),
            id:this.id,
            cd_fire:this._cd_fire,
            bonuses:this.bonuses,
            sprite_bullet:this.sprite_bullet
        };
        // return {
        //     id:this.id,
        //     x:this.x,
        //     y:this.y,
        //     angle:this.angle,
        //     max_hit_points:this.max_hit_points,
        //     hit_points:this.hit_points,
        //     speed_rot:this._speed_rot,
        //     cd_fire:this._cd_fire,
        //     tower_angle:this.tower.angle
        // };
    }

    toJson(){
        return this._toJson();
    }

    _borderCollision(){
        let correct = WorldBorder.colligion({
            x:this.x,
            y:this.y,
            obj:'tank'
        });
        if(correct.angle!=0){
            this.x = correct.x;
            this.y = correct.y;
        }
        // if(this._lvl_width_min>=this.x){
        //     //console.log('коллизия с полем слева');
        //     this.x = this._lvl_width_min;
        // }else if(this._lvl_width_max<=this.x){
        //     //console.log('коллизия с полем справа');
        //     this.x = this._lvl_width_max;
        // }

        // if(this._lvl_height_min>=this.y){
        //     //console.log('коллизия с полем сверху');
        //     this.y = this._lvl_height_min;
        // }else if(this._lvl_height_max<=this.y){
        //     //console.log('коллизия с полем снизу');
        //     this.y = this._lvl_height_max;
        // }
    }

    fire(){
        //console.log('услышал выстрел!!! = ', this.is_induced,'||', this._cd_fire);
        if(this._live && this.is_induced && !this._cd_fire){
            //console.log('выстрелил!!!');
            this._cd_fire = true;
            this.timer.addTimer(this._time_cd_fire,()=>{this._cd_fire=false});
            let angle = this.angle + this.tower.angle;
            let rad = (angle)*Math.PI/180;
            let kY = Math.sin(rad);
            let kX = Math.cos(rad);
			let posX = this.x - 12*kX + 0*kY;
            let posY = this.y - 12*kY + 0*kX;
            let sx = -this._speed_bullet * kX;
			let sy = -this._speed_bullet * kY;
            this.addBullet({
                sprite:this.sprite_bullet,
                str_obj:'tanks',
                obj_id:this.id,
                walls:this.walls,
                speed:this._speed_bullet,
                damage:this.damage,
                posX,
                posY,
                angle,
                sx,
                sy
            });

            if(this.sprite_bullet==='bullet_heal'){
                this.sprite_bullet = this._sprite_bullet;
            }
            //console.log('создал пулю');
        }
    }

    update(delta){
        if(!this._live){
            return;
        }
        if(this._speed_rot!=0){
            //this.angle += this._speed_rot;
            this.is_induced = false;
            if(this._speed!=0){
                this.angle += this._speed_rot/0.5;

            }else{
                this.angle += this._speed_rot;
            }
        }

        let angle = this.angle * Math.PI / 180;
        
        if(this._speed!=0){
            let corrector = null;
            this.is_induced = false;
            //let angle = this.angle * Math.PI / 180;
		    let xmov = this._speed * this._forse_x * Math.cos(angle);
            let ymov = this._speed * this._forse_y * Math.sin(angle);
            this.x+=xmov;
            this.y+=ymov;
            //console.log(this.x,'||',this.y);
            this._borderCollision();
            this.walls.forEach(wall => {
                let result = wall.colligion(this.x,this.y,'tank');
                if(result.forseX!=1 || result.forseY!=1){
                    corrector = result;
                    this.x = corrector.x;
                    this.y = corrector.y;
                    //this.num_colligion++;
                }
                //this.x = wall.colligion(this.x,this.y).x;
                //this.y = wall.colligion(this.x,this.y).y;
            });

            if(corrector){
                // this.cont.x = corrector.x;
                // this.cont.y = corrector.y;
                this._force_y = corrector.forseY;
                this._forse_x = corrector.forseX;
            }else{
                this._force_y = 1;
                this._forse_x = 1;
            }
        }

        
        if(this.client==='PC'){
            //console.log('должен обновить положение башни!!!');
            this.tower.update(angle);
        }else{
            this.tower.updateMob();
        }
    }
}