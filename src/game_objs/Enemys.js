const {random,makeId} = require('../serveces/Mth');
const Coronavirus = require('./Coronavirus');

module.exports = class Enemys{
    constructor({timer,walls,addBullet,addBonus,playersWon}){
        this.name = 'Coronavirus';
        this.timer = timer;
        this.walls = walls;
        this.addBullet = addBullet;
        this.addBonus = addBonus;
        this.playersWon = playersWon;
        this.enemys = [];
        this.num_enemys = 20;
        this.num_kill_enemys = 0;
        this.min_num_in_wave = 2;
        this.max_num_in_wave = 2;
        this.num_wave = 0;
        this.max_num_wave = 8;
        this.timer_wave = 0;
        this.min_time_wave = 40;
        this.max_time_wave = 50;
        this._l_pos = {x:0,y:0};
        this._t_pos = [
            {
                x:100,
                y:80
            },
            {
                x:820,
                y:80
            },
            {
                x:820,
                y:620
            },
            {
                x:100,
                y:620
            }
        ];
        this.damage_done = 0;
        this.damage_take = 0;
        this.heal_take = 0;
        this.heal_done = 0;
        this.num_kills = 0;
        this.is_wave = false;
        //this.init();
    }

    init(){
        this.is_wave = true;

        if(this.num_wave>=this.max_num_wave){
            return;
        }
        this.num_wave++;
        //console.log('добавляю вирус = ',this.num_wave);
        const num = random(this.min_num_in_wave,this.max_num_in_wave);
        let t_arr = [...this._t_pos];
        for(let i=0;i<num;i++){
            const pos = this._findPos(t_arr);
            this.enemys.push(new Coronavirus({
                factory:this,
                reason:'spawn',
                id: makeId(),
                ...pos,
                angle:0,
                walls:this.walls,
                timer:this.timer,
                addBullet:this.addBullet
            }));
        }
        this.timer.addTimer(1000*random(this.min_time_wave,this.max_time_wave),()=>{
            //console.log('отсчитал таймер добавления');
            this.is_wave = false;
        });
    }

    virusGrow(cell){
        //console.log('включил таймер!!!');
        this.timer.addTimer(1000*0.8,()=>{
            if(!cell){
                return;
            }
            const pos = this._findPosGrowCell(cell);
            this.enemys.push(new Coronavirus({
                factory:this,
                reason:'segmentation',
                id:makeId(),
                ...pos,
                angle:0,
                hit_points:cell.hit_points,
                walls:this.walls,
                timer:this.timer,
                addBullet:this.addBullet
            }));
            cell.grow = 0;
            cell.is_grow = false;
        });
        
    }

    setKill(){
        this.num_kills++;
    }

    killEnemy(id){
        const idx = this.findIdxEnemy(id);
        const rnd = random(0,100);
        if(rnd<=50){
            this.addBonus({
                x:this.enemys[idx].x,
                y:this.enemys[idx].y
            });
        }
        
        this.enemys.splice(idx,1);

        if(this.enemys.length<=0&&this.num_wave>=this.max_num_in_wave){
            this.playersWon();
        }
    }

    setDoneDamage({id,damage}){
        this.damage_done += damage;
        //const idx = this.findIdxEnemy(id);
        //this.enemys[idx].setDoneDamage(damage);
    }

    getEnemys(){
        let enemys = [];
        this.enemys.forEach((enemy)=>{
            enemys.push(enemy.toJson());
        });
        return enemys;
    }

    findIdxEnemy(id){
        return this.enemys.findIndex((enemy)=>{return enemy.id===id});
    }

    _findPosGrowCell(cell){
        const pos = {x:cell.x,y:cell.y};
        const rnd = random(0,1);
        if(rnd===0){
            cell.x-=5;
            pos.x+=5;
        }else{
            cell.y-=5;
            pos.y+=5;
        }

        return pos;
    }

    _findPos(arr_pos){
        let idx = Math.floor(Math.random() * arr_pos.length);
        let pos = arr_pos[idx];
        arr_pos.splice(idx,1);
        //if(this._l_pos.x!=this._t_pos[idx].x && this._l_pos.y!=this._t_pos[idx].y){
            //this._l_pos = this._t_pos[idx];
            //return this._l_pos;
        //}
        return pos;
    }

    toLader(){
        return{
            id:this.name,
            name:this.name,
            damage_done:this.damage_done,
            damage_take:this.damage_take,
            heal_done:this.heal_done,
            heal_take:this.heal_take,
            num_kills:this.num_kills
        }
    }

    update(){
        if(!this.is_wave){
            this.init();
        }
        this.enemys.forEach((enemy)=>{
            enemy.update();
        });
    }
}