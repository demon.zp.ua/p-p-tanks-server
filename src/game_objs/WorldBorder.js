class WorldBorder{
    constructor(){
        this._lvl_width_min = 0;
        this._lvl_width_max = 0;
        this._lvl_height_min = 0;
        this._lvl_height_max = 0;
        this.bullet = 0;
        this.tank = 0;
    }

    init({
        width_min,
        width_max,
        height_min,
        height_max,
        width_bullet,
        width_tank
    }){
        this._lvl_width_max = width_max;
        this._lvl_width_min = width_min;
        this._lvl_height_min = height_min;
        this._lvl_height_max = height_max;
        this.bullet = width_bullet;
        this.tank = width_tank;
    }

    colligion({x,y,obj}){
        //console.log(obj);
        let angle = 0;
        let w_min = this._lvl_width_min+this[obj];
        let w_max = this._lvl_width_max-this[obj];
        let h_min = this._lvl_height_min+this[obj];
        let h_max = this._lvl_height_max-this[obj];
        if(w_min>=x){
            //console.log('коллизия с полем слева');
            x = w_min;
            angle = 90;
        }else if(w_max<=x){
            //console.log('коллизия с полем справа');
            x = w_max;
            angle = 90;
        }

        if(h_min>=y){
            //console.log('коллизия с полем сверху');
            y = h_min;
            angle = 180;
        }else if(h_max<=y){
            //console.log('коллизия с полем снизу');
            y = h_max;
            angle = 180;
        }

        return {
            x,
            y,
            angle
        }
    }
}

const worldBorder = new WorldBorder();
module.exports = worldBorder;