module.exports = class GameObject{
    constructor({x = 0,y = 0,angle = 0,type}){
        this.x = x;
        this.y = y;
        this.angle = angle;
        this.type = type;
    }
}