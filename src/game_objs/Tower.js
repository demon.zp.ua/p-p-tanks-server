module.exports = class Tower{
    constructor(parent){
        this.parent = parent;
        this.x = 0;
        this.y = 0;
        this._tower_rot_speed = 1.92;
        this.angle = 0;
        this.dAngle = 0;
        this.rotCorect = 0;
        //this.is_induced = false;
    }

    updateMob(){
    	if(!this.parent.is_induced){
			if (this.dAngle >= 180) {
				this.dAngle = -360 + this.dAngle;
			} else if (this.dAngle < -180) {
				this.dAngle = 360 + this.dAngle;
			}

			if(Math.abs(this.dAngle) <= this._tower_rot_speed){
				//console.log('навелся!!!');
				this.dAngle = 0;
				this.parent.is_induced = true;
				//this._tower_angle -= this.dAngle;
			}else if(this.dAngle > 0){
				//console.log('НЕ навелся!!!');
				this.parent.is_induced = false;
				this.dAngle -= this._tower_rot_speed
				this.angle -= this._tower_rot_speed;
			}else if(this.dAngle < 0){
				//console.log('НЕ навелся!!!');
				this.parent.is_induced = false;
				this.dAngle += this._tower_rot_speed;
				this.angle += this._tower_rot_speed;
			}
		}
    }

    update(parent_angle){
        if(!this.parent.is_induced){
            //let angle = this.parent.angle * Math.PI / 180;
            //console.log('прощитую башню!!!');
            let kY = Math.sin(parent_angle);
            let kX = Math.cos(parent_angle);

            let real_x = this.parent.x +this.x*kX + this.y*kY;
            let real_y = this.parent.y +this.x*kY + this.y*kX;
			let mDx2 = real_x - this.parent.target_pos.x;
			let mDy2 = real_y - this.parent.target_pos.y;
			let mAngle = Math.atan2(mDy2, mDx2);
			//получаем угол между мышкой и башней в градусах
			let mAngle2 = mAngle / Math.PI * 180;
			//сколько градусов нехватает для полного поворота на мышь
			let dAngle = this.parent.angle + this.angle - mAngle2 + this.rotCorect;
			
				if (dAngle >= 180) {
					dAngle = -360 + dAngle;
				} else if (dAngle < -180) {
					dAngle = 360 + dAngle;
				}
           

			if(Math.abs(dAngle) < this._tower_rot_speed){
				//console.log("приехали");
                dAngle = 0;
                this.parent.is_induced = true;
				
				this._tower_angle -= dAngle;
				//console.log("this.obj.angle= ", this.obj.angle);
			}else if(dAngle > 0){
				this.parent.is_induced = false;
				//trace("dAngleD2 > 0");
				this.angle -= this._tower_rot_speed;
			}else if(dAngle < 0){
				this.parent.is_induced = false;
				//trace("dAngleD2 < 0");
				this.angle += this._tower_rot_speed;
			}

			
       }
    }
}