const WorldBorder = require('./WorldBorder');
const GameObject = require('./GameObject');
const {random} = require('../serveces/Mth');

module.exports = class Coronavirus extends GameObject{
    constructor({
                    factory,
                    reason,
                    id,
                    x,
                    y,
                    angle,
                    hit_points,
                    timer,
                    walls,
                    addBullet
                }){
        super({
                x,
                y,
                angle,
                type:'virus'
            });
        this.reason = reason;
        this.factory = factory;
        this.id = 'c'+id;
        this.timer = timer;
        this.walls = walls;
        this.addBullet = addBullet;
        this.max_hit_points = 5;
        this.hit_points = hit_points || this.max_hit_points;
        this.percent_hp = this.hit_points*100/this.max_hit_points;
        this.grow = 0;
        this.is_grow = false;
        this._on_dot = false;
        this.target_pos = {x:0,y:0};
        this._angle = 0;
        this._live = true;
        //this.is_forward = false;
        //this.is_back = false;
        this._main_speed = 0.76;
        this._speed = 0;
        this._speed_bullet = 4.6;
        this.damage = 1;
        this._forse_y = 1;
        this._forse_x = 1;
        this.is_induced = false;
        this._enemys = [];
        this.allies = [];
        this._cd_fire = true;
        this._time_cd_fire = 1000*3.6;
        this.sx = 0;
        this.sy = 0;
        this.is_init = false;
        this._sprite_bullet = 'bullet_c1';
        this.sprite_bullet = this._sprite_bullet;
        //this.rotCorect = 180;
        this.init();
    }

    init(){
        this.timer.addTimer(1000*0.8,()=>{this.is_init=true});
        this.timer.addTimer(this._time_cd_fire,()=>{this._cd_fire=false});
    }

    setHeal({from,damage}){
        let t_heal = damage;
        this.hit_points += damage;

        if(this.hit_points>this.max_hit_points){
            t_heal -= this.hit_points - this.max_hit_points;
            this.hit_points = this.max_hit_points;
        }

        this.percent_hp = this.hit_points*100/this.max_hit_points;

        this.factory.heal_take += t_heal;
        return t_heal;
    }

    setDamage({from,damage}){
        //console.log('idx = ',this.factory.findIdxEnemy(from));
        if(this.factory.findIdxEnemy(from)>=0){
            this.setToGrow();
            return 0;
        }
        let t_damage = damage;
        this.hit_points -= damage;

        if(this.hit_points<0){
            t_damage += this.hit_points;
        }

        this.factory.damage_take += t_damage;
        this.percent_hp = this.hit_points*100/this.max_hit_points;
        //console.log('damage = ', damage);
        return t_damage;
    }

    setToGrow(){
        if(this.grow<4){
            this.grow++;
        }
        
        if(this.grow>=4){
            if(!this.is_grow){
                this.is_grow = true;
                this.factory.virusGrow(this);
            }
        }
    }

    isKill(){
        if(this.hit_points<=0){
            this._live = false;
            return true;
        }

        return false;
    }

    addEnemy(user_id){
        this._enemys.push(user_id);
    }

    addAlly(user_id){
        this.allies.push(user_id);
    }

    toLader(){
        return{
            id:this.id,
            name:this.name,
            damage_done:this.damage_done,
            damage_take:this.damage_take,
            num_kills:this.num_kills
        }
    }

    _toJson(){
        return {
            id:this.id,
            x:this.x,
            y:this.y,
            angle:this.angle,
            grow:this.grow,
            type:this.type,
            reason:this.reason,
            max_hit_points:this.max_hit_points,
            speed_rot:this._speed_rot,
            speed:this._speed,
            hit_points:this.hit_points,
            percent_hp:this.percent_hp
        };
    }

    toJsonPl(){
        return {
            ...this._toJson(),
            cd_fire:this._cd_fire
        };
        // return {
        //     id:this.id,
        //     x:this.x,
        //     y:this.y,
        //     angle:this.angle,
        //     max_hit_points:this.max_hit_points,
        //     hit_points:this.hit_points,
        //     speed_rot:this._speed_rot,
        //     cd_fire:this._cd_fire,
        //     tower_angle:this.tower.angle
        // };
    }

    toJson(){
        return this._toJson();
    }

    _borderCollision(){
        let correct = WorldBorder.colligion({
            x:this.x,
            y:this.y,
            obj:'tank'
        });
        if(correct.angle!=0){
            this.x = correct.x;
            this.y = correct.y;
            this._on_dot = false;
        }

    }

    fire(){
        //console.log('выстрелил!!!');
        this._cd_fire = true;
        this.timer.addTimer(this._time_cd_fire,()=>{this._cd_fire=false});
        this._angle = random(0,360);
        for(let i=0;i<2;i++){
            let angle = this._angle;
            if(i>0){
                angle+=180;
            }
            let rad = (angle)*Math.PI/180;
            let kY = Math.sin(rad);
            let kX = Math.cos(rad);
            let posX = this.x - 12*kX + 0*kY;
            let posY = this.y - 12*kY + 0*kX;
            let sx = -this._speed_bullet * kX;
            let sy = -this._speed_bullet * kY;
            this.addBullet({
                sprite:this.sprite_bullet,
                str_obj:'enemys',
                obj_id:this.id,
                walls:this.walls,
                speed:this._speed_bullet,
                damage:this.damage,
                posX,
                posY,
                angle,
                sx,
                sy
            });
        }
    }

    goToTarget(){
        this.x+=this.sx;
        this.y+=this.sy;
        const r = Math.sqrt((this.target_pos.x - this.x)*(this.target_pos.x - this.x) + 
                               (this.target_pos.y - this.y)*(this.target_pos.y - this.y));
        if(r<=3){
            this._on_dot = false;
        }    
    }

    _setVectorSpeed(){
        const xdiff = this.x - this.target_pos.x;
        const ydiff = this.y - this.target_pos.y;
        const alfa = Math.atan2(ydiff, xdiff);
        this.sx = this._main_speed * Math.cos(alfa);
		this.sy = this._main_speed * Math.sin(alfa);
    }

    _getDot(){
        this.target_pos.x = this.x + random(-100,100);
        this.target_pos.y = this.y + random(-100,100);
        
        this._on_dot = true;
        this._setVectorSpeed();
    }

    update(delta){
        if(!this._live || !this.is_init){
            return;
        }

        if(!this._on_dot){
            this._getDot();
        }else if(!this.is_grow){
            this.goToTarget();
            this._borderCollision();
        }

        if(!this._cd_fire && !this.is_grow){
            this.fire();
        }
        
    }
}