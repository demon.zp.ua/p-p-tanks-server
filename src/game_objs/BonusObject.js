
module.exports = class BonusObject{
    constructor({
        id,
        tanks,
        x,
        y,
        bonus,
        delBonus
    }){
        this.tanks = tanks,
        this.id = id;
        this.x = x;
        this.y = y;
        this.bonus = bonus;
        this.delBonus = delBonus;
        this.summ_r = 30;
        //console.log('выстрелил!!! = ', angle);
    }

    _tankCollision(tank){
        if(!tank._live){
            return;
        }
        let r1 = Math.sqrt((tank.x - this.x)*(tank.x - this.x) + 
                (tank.y - this.y)*(tank.y - this.y));
        //console.log('ищю столкновение с = ', tank.id);
        if(r1<=this.summ_r){
            //console.log('должен подобрать бонус');
            tank.addBonus(this.bonus);
            this.delBonus(this.id);
        }
    }

    toJson(){
        return {
            id:this.id,
            x:this.x,
            y:this.y,
            bonus:this.bonus
        }
    }

    update(delta){
        for (const key in this.tanks) {
            this._tankCollision(this.tanks[key]);
        }
    }
}