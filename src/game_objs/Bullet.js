const WorldBorder = require('./WorldBorder');

module.exports = class Bullet{
    constructor({
        sprite,
        obj_id,
        walls,
        tanks,
        id,
        angle,
        posX,
        posY,
        sx,
        sy,
        speed,
        damage,
        delBullet,
        killTank
    }){
        this.sprite = sprite;
        this.id = obj_id+id;
        this.tank_id = obj_id,
        this.walls = walls;
        this.tanks = tanks;
        this.r = 5;
        this.tank_r = 20;
        this.summ_r = this.r + this.tank_r;
        this.x = posX;
        this.y = posY;
        this.speed = speed;
        this.sx = sx;
        this.sy = sy;
        this.angle = angle;
        this.num_bounces = 0;
        this.max_num_bounces = 3;
        this.delBullet = delBullet;
        this.killTank = killTank;
        this.damage = damage;

        //console.log('выстрелил!!! = ', angle);
    }

    _borderCollision(){
        const correct = WorldBorder.colligion({
            x:this.x,
            y:this.y,
            obj:'bullet'
        });
        if(correct.angle!=0){
            this.x = correct.x;
            this.y = correct.y;
            this.reactionCollision(correct.angle);
        }
        // if(this._lvl_width_min>=this.x){
        //     //console.log('коллизия с полем слева');
        //     this.x = this._lvl_width_min;
        //     this.reactionCollision(90);
            
        // }else if(this._lvl_width_max<=this.x){
        //     //console.log('коллизия с полем справа');
        //     this.x = this._lvl_width_max;
        //     this.reactionCollision(90);
        // }

        // if(this._lvl_height_min>=this.y){
        //     //console.log('коллизия с полем сверху');
        //     this.y = this._lvl_height_min;
        //     this.reactionCollision(180);
        // }else if(this._lvl_height_max<=this.y){
        //     //console.log('коллизия с полем снизу');
        //     this.y = this._lvl_height_max;
        //     this.reactionCollision(180);
        // }
    }

    _tankCollision(tank){
        if(!tank._live){
            return;
        }
        let r1 = Math.sqrt((tank.x - this.x)*(tank.x - this.x) + 
                (tank.y - this.y)*(tank.y - this.y));
        
        if(r1<=this.summ_r){
            if(this.tank_id!=tank.id){
                this._damageCalculation(tank);
            }else{
                if(this.num_bounces>=1){
                    this._damageCalculation(tank);
                }
            }
            
        }
    }

    _damageCalculation(tank){
        const t_damage = tank.setDamage(this.damage);
        const is_kill = tank.isKill();
        this.tanks[this.tank_id].setDoneDamage(t_damage);
        if(is_kill){
            this.tanks[this.tank_id].setKill();
            this.killTank(this.id);
        }
        this.delBullet(this.id);
    }

    reactionCollision(a){
        if(this.num_bounces>=this.max_num_bounces){
            this.delBullet(this.id);
        }
        this.damage = this.damage*2;
        this.num_bounces++;
        let angle = a*2-this.angle;
        this.angle = angle;
        let rad = (angle)*Math.PI/180;
        let kY = Math.sin(rad);
        let kX = Math.cos(rad);
        this.sx = -6 * kX;
		this.sy = -6 * kY;
    }

    toJson(){
        return {
            id:this.id,
            x:this.x,
            y:this.y,
            sprite:this.sprite
        }
    }

    update(delta){
        this.x+=this.sx;
        this.y+=this.sy;
        this._borderCollision();
        
        // this.tanks.forEach(tank=>{
        //     this._tankCollision(tank);
        // });

        for (const key in this.tanks) {
            this._tankCollision(this.tanks[key]);
        }
        //let corrector = null;
        this.walls.forEach(wall => {
            let result = wall.colligion(this.x,this.y,'bullet');
            //if(result.forseX!=1 || result.forseY!=1){
                //corrector = result;
                
                if(result.forseX!=1){
                    this.x = result.x;
                    this.y = result.y;
                    this.reactionCollision(180);
                }
                if(result.forseY!=1){
                    this.x = result.x;
                    this.y = result.y;
                    this.reactionCollision(90);
                }
                //this.num_colligion++;
            //}
            //this.x = wall.colligion(this.x,this.y).x;
            //this.y = wall.colligion(this.x,this.y).y;
        });
    }
}