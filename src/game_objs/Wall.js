module.exports = class Wall{
    constructor({x,y,w,h}){
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        //this.width_half = 40;
        //this.height_half = 40;

        this.w_h_tank = 20+this.w;
        this.h_h_tank = 20+this.h;

        this.x0_tank = this.x-this.w_h_tank;
        this.x1_tank = this.x+this.w_h_tank;
        this.y0_tank = this.y-this.h_h_tank;
        this.y1_tank = this.y+this.h_h_tank;

        this.dotx1_tank = {x:this.x0_tank,y:this.y0_tank+10};
        this.dotx2_tank = {x:this.x0_tank,y:this.y1_tank-10};

        this.dotx3_tank = {x:this.x1_tank,y:this.y0_tank+10};
        this.dotx4_tank = {x:this.x1_tank,y:this.y1_tank-10};

        this.dotx3_1_tank = {x:this.x1_tank-10,y:this.y0_tank+10};
        this.dotx4_1_tank = {x:this.x1_tank-10,y:this.y1_tank-10};

        this.doty1_tank = {x:this.x0_tank+10,y:this.y0_tank};
        this.doty2_tank = {x:this.x0_tank+10,y:this.y1_tank};

        this.doty3_tank = {x:this.x1_tank-10,y:this.y0_tank};
        this.doty4_tank = {x:this.x1_tank-10,y:this.y1_tank};

        this.doty2_1_tank = {x:this.x0_tank+10,y:this.y1_tank-10};
        this.doty4_1_tank = {x:this.x1_tank-10,y:this.y1_tank-10};

        this.w_h_bullet = 5+this.w;
        this.h_h_bullet = 5+this.h;
        this.x0_bullet = this.x-this.w_h_bullet;
        this.x1_bullet = this.x+this.w_h_bullet;
        this.y0_bullet = this.y-this.h_h_bullet;
        this.y1_bullet = this.y+this.h_h_bullet;

        this.dotx1_bullet = {x:this.x0_bullet,y:this.y0_bullet+10};
        this.dotx2_bullet = {x:this.x0_bullet,y:this.y1_bullet-10};

        this.dotx3_bullet = {x:this.x1_bullet,y:this.y0_bullet+10};
        this.dotx4_bullet = {x:this.x1_bullet,y:this.y1_bullet-10};

        this.dotx3_1_bullet = {x:this.x1_bullet-10,y:this.y0_bullet+10};
        this.dotx4_1_bullet = {x:this.x1_bullet-10,y:this.y1_bullet-10};

        this.doty1_bullet = {x:this.x0_bullet+10,y:this.y0_bullet};
        this.doty2_bullet = {x:this.x0_bullet+10,y:this.y1_bullet};

        this.doty3_bullet = {x:this.x1_bullet-10,y:this.y0_bullet};
        this.doty4_bullet = {x:this.x1_bullet-10,y:this.y1_bullet};

        this.doty2_1_bullet = {x:this.x0_bullet+10,y:this.y1_bullet-10};
        this.doty4_1_bullet = {x:this.x1_bullet-10,y:this.y1_bullet-10};
    }

    colligion(x,y,obj){
        let colligionX = this.colligionX(x,y,obj);
        let colligionY = this.colligionY(x,y,obj);
        return {...colligionX,...colligionY};
    }

    colligionX(x,y,obj){
        let Dx = this[`dotx2_${obj}`].x - this[`dotx1_${obj}`].x;
		let Dy = this[`dotx2_${obj}`].y - this[`dotx1_${obj}`].y;
        let d1 = ((this[`dotx1_${obj}`].y - y) * Dx + (x - this[`dotx1_${obj}`].x) * Dy ) / (Dy * Dy + Dx * Dx);

		if(d1<0){
            //console.log('xd1<0');
            return {x,forseY:1};
        }

        Dx = this[`dotx3_${obj}`].x - this[`dotx1_${obj}`].x;
        Dy = this[`dotx3_${obj}`].y - this[`dotx1_${obj}`].y;

		let d2 = ((this[`dotx1_${obj}`].y - y) * Dx + (x - this[`dotx1_${obj}`].x) * Dy ) / (Dy * Dy + Dx * Dx);

        if(d2>0){
            return {x,forseY:1};
        }

        Dx = this[`dotx2_${obj}`].x - this[`dotx4_${obj}`].x;
		Dy = this[`dotx2_${obj}`].y - this[`dotx4_${obj}`].y;
						
        let d3 = ((this[`dotx4_${obj}`].y - y) * Dx + (x - this[`dotx4_${obj}`].x) * Dy ) / (Dy * Dy + Dx * Dx);
        if(d3>0){
            return {x,forseY:1};
        }

        Dx = this[`dotx3_1_${obj}`].x - this[`dotx4_1_${obj}`].x;
		Dy = this[`dotx3_1_${obj}`].y - this[`dotx4_1_${obj}`].y;
						
		let d4_1 = ((this[`dotx4_1_${obj}`].y - y) * Dx + (x - this[`dotx4_1_${obj}`].x) * Dy ) / (Dy * Dy + Dx * Dx);

        // if(d1>0 && d2<0 && d3<0 && d4_1>0){
        //     return {x:this.x0,forseY:0.4};
        // }

        if(d4_1>0){
            return {x:this[`x0_${obj}`],forseY:0.4};
        }

        Dx = this[`dotx3_${obj}`].x - this[`dotx4_${obj}`].x;
		Dy = this[`dotx3_${obj}`].y - this[`dotx4_${obj}`].y;
						
        let d4 = ((this[`dotx4_${obj}`].y - y) * Dx + (x - this[`dotx4_${obj}`].x) * Dy ) / (Dy * Dy + Dx * Dx);

        // if(d4>0 && d2<0 && d3<0 && d1>0){
        //     return {x:this.x1,forseY:0.4};
        // }
        if(d4>0){
            return {x:this[`x1_${obj}`],forseY:0.4};
        }

        return {x,forseY:1};
    }

    colligionY(x,y,obj){
        let Dx = this[`doty3_${obj}`].x - this[`doty1_${obj}`].x;
		let Dy = this[`doty3_${obj}`].y - this[`doty1_${obj}`].y;
        let d2 = ((this[`doty1_${obj}`].y - y) * Dx + (x - this[`doty1_${obj}`].x) * Dy ) / (Dy * Dy + Dx * Dx);
        if(d2>0){
            //console.log('yd2>0');
            return {y,forseX:1};
        }
        // Dx = this.dot2_1.x - this.dot1_1.x;
        // Dy = this.dot2_1.y - this.dot1_1.y;
        
        // let d1_1 = ((this.dot1_1.y - y) * Dx + (x - this.dot1_1.x) * Dy ) / (Dy * Dy + Dx * Dx);

        Dx = this[`doty2_${obj}`].x - this[`doty1_${obj}`].x;
		Dy = this[`doty2_${obj}`].y - this[`doty1_${obj}`].y;
						
		let d1 = ((this[`doty1_${obj}`].y - y) * Dx + (x - this[`doty1_${obj}`].x) * Dy ) / (Dy * Dy + Dx * Dx);
        if(d1<0){
            return {y,forseX:1};
        }
        Dx = this[`doty3_${obj}`].x - this[`doty4_${obj}`].x;
		Dy = this[`doty3_${obj}`].y - this[`doty4_${obj}`].y;
						
        let d3 = ((this[`doty4_${obj}`].y - y) * Dx + (x - this[`doty4_${obj}`].x) * Dy ) / (Dy * Dy + Dx * Dx);
        if(d3<0){
            return {y,forseX:1};
        }
        
        Dx = this[`doty2_1_${obj}`].x - this[`doty4_1_${obj}`].x;
		Dy = this[`doty2_1_${obj}`].y - this[`doty4_1_${obj}`].y;
						
		let d4_1 = ((this[`doty4_1_${obj}`].y - y) * Dx + (x - this[`doty4_1_${obj}`].x) * Dy ) / (Dy * Dy + Dx * Dx);

        // if(d2<0 && d1>0 && d3>0 && d4_1<0){
        //     return {y:this.y0,forseX:0.4};
        // }
        if(d4_1<0){
            return {y:this[`y0_${obj}`],forseX:0.4};
        }

        Dx = this[`doty2_${obj}`].x - this[`doty4_${obj}`].x;
		Dy = this[`doty2_${obj}`].y - this[`doty4_${obj}`].y;
						
        let d4 = ((this[`doty4_${obj}`].y - y) * Dx + (x - this[`doty4_${obj}`].x) * Dy ) / (Dy * Dy + Dx * Dx);

        // if(d4<0 && d1>0 && d3>0 && d2<0){
        //     return {y:this.y1,forseX:0.4};
        // }
        if(d4<0){
            return {y:this[`y1_${obj}`],forseX:0.4};
        }

        return {y,forseX:1};
    }
}