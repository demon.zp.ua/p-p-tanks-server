const express = require('express');
const app = express();
const path = require('path');
const http = require('http').createServer(app);
const io = require('socket.io')(http);

const PORT = process.env.PORT || 3000;

const {userSocketEvents} = require('./src/events/socket_events/user');
const GamesService = require('./src/serveces/gamesService');
const RoomsService = require('./src/serveces/roomsService');
const UsersService = require('./src/serveces/usersService');
const Game = require('./src/game');

const gamesService = new GamesService(io);
const roomsService = new RoomsService(io);
const usersService = new UsersService(io);

app.use(express.static(path.join(__dirname, 'public')));
// app.get('/', function(req, res){
//     res.sendFile(__dirname + '/public/index.html');
// });

io.on('connection', (socket)=>{
    socket.emit('is_connect', {msg:'есть коннект'});
    socket.on('join', (data) => {
        if (!data.name||!data.password){
            socket.emit('join', {errors: ['login and password required']});
            return;
        }
        data['socket'] = socket;
        const user = usersService.authUser(data);

        if (user===undefined) {
            socket.emit('join', {errors: ['login or password is wrong']});
            return;
        }
        userSocketEvents(socket, roomsService, usersService, gamesService);
        socket.emit('join', {msg:'есть коннект', data:user.toJson()});
        usersService.toRoom(user);
        //console.log('успешно законектился');
    });
});

http.listen(PORT, function(){
  console.log(`Server up in port: ${PORT}`);
});

//let game = new Game({id:'1',players:[1,2]});

//gamesService.createGame({id:'1',players:[1,2]});

//let timerId = setInterval(() => {console.log("2 секунды ");}, 2000);

/*let timerRooms = setInterval(() => {
    roomsService.update();
}, 1000);*/